<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Registro extends Model
{
    protected $fillable = ['nombre','apellido','domicilio','documento','nacionalidad','email','como_llego','equipo','comentario','nacimiento','telefono'];
}
