<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>ARECO 10K - SUMATE</title>

        <link href="{{ asset('/css/bootstrap.min.css') }}" rel="stylesheet" />
        <link href="{{ asset('/css/app.css') }}" rel="stylesheet" />
    </head>
    <body>
        <header>
            <img src="{{ asset('/img/logo.jpg') }}" class="main-logo" alt="Logo Municipio de San Antonio de Areco" />

            <h1 class="text-center">¡Sumate a la carrera!</h1>
            <p class="text-center">Sumate a la carrera 10K entre el campo y la ciudad.</p>
        </header>
        <div class="container">

            @if(session()->has('message'))
                <div class="jumbotron mt-5">
                    <h1 class="text-center">{{ session()->get('message') }}</h1>
                    <p class="text-center">Tu número de corredor es el: SAA-{{ session()->get('id') }}</p>
                </div>
            @else
                <form action="agregar" method="POST">
                    <div class="mt-4">
                        <label for="nombre">Nombre *</label>
                    <input type="text" name="nombre" id="nombre" class="form-control" value="{{ old('nombre') }}"/>
                        {{ $errors->first('nombre') }}
                    </div>
                    <div class="mt-4">
                        <label for="apellido">Apellido *</label>
                        <input type="text" name="apellido" id="apellido" class="form-control" value="{{ old('apellido') }}"/>
                        {{ $errors->first('apellido') }}
                    </div>
                    <div class="mt-4">
                        <label for="nacimiento">Fecha de nacimiento *</label>
                        <input type="date" name="nacimiento" id="nacimiento" class="form-control" value="{{ old('nacimiento') }}"/>
                        {{ $errors->first('nacimiento') }}
                    </div>
                    <div class="mt-4">
                        <label for="domicilio">Domicilio Completo <span>(Calle, número, ciudad, provincia, país)</span></label>
                        <input type="text" name="domicilio" id="domicilio" class="form-control" value="{{ old('domicilio') }}"/>
                        {{ $errors->first('domicilio') }}
                    </div>
                    <div class="mt-4">
                        <label for="documento">Nº Documento / Pasaporte *</label>
                        <input type="text" name="documento" id="documento" class="form-control" value="{{ old('documento') }}"/>
                        {{ $errors->first('documento') }}
                    </div>
                    <div class="mt-4">
                        <label for="nacionalidad">Nacionalidad</label>
                        <input type="text" name="nacionalidad" id="nacionalidad" class="form-control" value="{{ old('nacionalidad') }}"/>
                        {{ $errors->first('nacionalidad') }}
                    </div>
                    <div class="mt-4">
                        <label for="email">Email *</label>
                        <input type="text" name="email" id="email" class="form-control" value="{{ old('email') }}"/>
                        {{ $errors->first('email') }}
                    </div>
                    <div class="mt-4">
                        <label for="telefono">Telefono</label>
                        <input type="text" name="telefono" id="telefono" class="form-control" value="{{ old('telefono') }}"/>
                        {{ $errors->first('telefono') }}
                    </div>
                    <div class="mt-4">
                        <label for="como_llego">¿Cómo te enteraste del evento?</label>
                        <select name="como_llego" id="como_llego" class="form-control">
                            <option value="Email">Email</option>
                            <option value="Publicidad en la calle">Publicidad en la calle</option>
                            <option value="Publicidad en la radio">Publicidad en la radio</option>
                        </select>
                        {{ $errors->first('como_llego') }}
                    </div>
                    <div class="mt-4">
                        <label for="equipo">¿En qué equipo querés participár?</label>
                        <select name="equipo" id="equipo" class="form-control">
                            <option value="rojo">Equipo Rojo</option>
                            <option value="verde">Equipo Verde</option>
                            <option value="indistinto">Indistinto</option>
                        </select>
                        {{ $errors->first('equipo') }}
                    </div>
                    <div class="mt-4">
                        <label for="comentario">Comentarios</label>
                        <textarea name="comentario" id="comentario" class="form-control">{{ old('comentario') }}</textarea>
                        {{ $errors->first('comentario') }}
                    </div>
                    <div class="mt-4 text-right">
                        <button class="btn btn-primary">Enviar</button>
                    </div>
                    {{ csrf_field() }}
                </form>
            @endif
        </div>

        <footer class="mt-5">
            <div class="container">
                Municipalidad de San Antonio de Areco - 2019
            </div>
        </footer>

        <script src="{{ asset('/js/jquery-3.4.1.min.js') }}"></script>
        <script src="{{ asset('/js/bootstrap.min.js') }}"></script>
    </body>
</html>
