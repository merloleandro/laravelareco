<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>ARECO 10K - LISTADO</title>

        <link href="{{ asset('/css/bootstrap.min.css') }}" rel="stylesheet" />
        <link href="{{ asset('/css/app.css') }}" rel="stylesheet" />
    </head>
    <body>
        <header>
            <img src="{{ asset('/img/logo.jpg') }}" class="main-logo" alt="Logo Municipio de San Antonio de Areco" />

            <h1 class="text-center mb-4">Lista de participantes</h1>
            <!-- <p class="text-center">Sumate a la carrera 10K entre el campo y la ciudad.</p> -->
        </header>

        <div class="container">
        
            @foreach($registrados as $registrado)
                <div class="row">
                    <div class="col-6 text-right">Nombre y apellido</div>
                    <div class="col-6">{{{ $registrado->nombre . " " . $registrado->apellido }}}</div>
                </div>
                @if($registrado->domicilio)
                <div class="row">
                    <div class="col-6 text-right">Domicilio</div>
                    <div class="col-6">{{{ $registrado->domicilio }}}</div>
                </div>
                @endif
                <div class="row">
                    <div class="col-6 text-right">Documento / Pasaporte</div>
                    <div class="col-6">{{{ $registrado->documento }}}</div>
                </div>
                @if($registrado->nacionalidad)
                <div class="row">
                    <div class="col-6 text-right">Nacionalidad</div>
                    <div class="col-6">{{{ $registrado->nacionalidad }}}</div>
                </div>
                @endif
                <div class="row">
                    <div class="col-6 text-right">E-Mail</div>
                    <div class="col-6">{{{ $registrado->email }}}</div>
                </div>
                @if($registrado->como_llego)
                <div class="row">
                    <div class="col-6 text-right">Como te enteraste?</div>
                    <div class="col-6">{{{ $registrado->como_llego }}}</div>
                </div>
                @endif
                @if($registrado->equipo)
                <div class="row">
                    <div class="col-6 text-right">Equipo</div>
                    <div class="col-6">{{{ $registrado->equipo }}}</div>
                </div>
                @endif
                @if($registrado->comentario)
                <div class="row">
                    <div class="col-6 text-right">Comentario</div>
                    <div class="col-6">{{{ $registrado->comentario }}}</div>
                </div>
                @endif
                @if($registrado->nacimiento)
                <div class="row">
                    <div class="col-6 text-right">Fecha de nacimiento</div>
                    <div class="col-6">{{{ $registrado->nacimiento }}}</div>
                </div>
                @endif
                @if($registrado->telefono)
                <div class="row">
                    <div class="col-6 text-right">Teléfono</div>
                    <div class="col-6">{{{ $registrado->telefono }}}</div>
                </div>
                @endif
                <hr />
            @endforeach
        </div>

        <footer class="mt-5">
            <div class="container">
                Municipalidad de San Antonio de Areco - 2019
            </div>
        </footer>

        <script src="{{ asset('/js/jquery-3.4.1.min.js') }}"></script>
        <script src="{{ asset('/js/bootstrap.min.js') }}"></script>
    </body>
</html>
