<?php

use Illuminate\Http\Request;
use App\Registro;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('form');
});

Route::post('/agregar', function (request $request) {

    $validator = Validator::make($request->all(), [
        'nombre' => 'required|max:255|alpha',
        'apellido' => 'required|max:255|alpha',
        'domicilio' => 'regex:([A-Z0-9 ])|nullable',
        'documento' => 'required|unique:registros|numeric',
        'nacionalidad' => 'regex:([A-Z ])|nullable',
        'email' => 'required|email',
        'nacimiento' => 'date|required',
        'telefono' => 'regex:([0-9])|nullable',
    ]);

    if ($validator->fails()) {
        return redirect('/')
        ->withInput()
        ->withErrors($validator);
    }

    $inputs = $request->all();

    foreach($inputs as $key => $value){
        $inputs[$key] = strip_tags($value);
    }

    $registro = Registro::create($inputs);

    return redirect()->back()->with('message', 'Felicitaciones, te esperamos el 01/07 a las 10AM!')->with('id', $registro->id);
});

Route::get('/ver', function () {
    $registrados = Registro::all();

    return view('registrados', [
        'registrados' => $registrados
    ]);
});
